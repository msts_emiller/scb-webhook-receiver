'use strict';

const dependencyService = require('./dependency-service');

// Accepts a config-key or array of config keys, and returns a promise that
// resolves to the the value(s) for the current stage (falls-back to dev)
// configured in ssm
module.exports = {
  config (keys) {
    const paramNames = mapKeys(keys);

    return getParams(paramNames)
      .then(ssmParameters => extractKeys(keys, ssmParameters))
      .then((parameters) => {
        // Get full parameter names for those parameters we're missing:
        const missingParams = keys.map((key, idx) => parameters[key] == null ? paramNames[idx] : null) // [null, 'scbi-webhook-receiver.staging.db-password', null]
                                  .filter(name => name != null);                                       // ['scbi-webhook-receiver.staging.db-password']

        if (missingParams.length > 0) {
          return Promise.reject(
            new Error('Missing the following required SSM parameters: ' + missingParams.join(', '))
          );
        }

        return parameters;
      });
  },
  internals: {
    getParams,
    mapKeys,
    extractKeys
  }
};

// Same as config, except it does not check for any missing keys in the result
module.exports.optional = function optionalConfig (keys) {
  const paramNames = mapKeys(keys);

  return getParams(paramNames)
    .then(ssmParameters => extractKeys(keys, ssmParameters));
};

// Translate keys into SSM parameter names
function mapKeys (keys) {
  if (typeof keys === 'string') {
    keys = [keys];
  }

  let stage = 'staging';
  const stageName = process.env.stageName;
  if (stageName === 'prod') {
    stage = stageName;
  }

  return keys.map(k => `scbi.${stage}.${k}`); // as they appear in SSM
}

// Request SSM parameters
function getParams (paramNames) {
  const ssm      = dependencyService('awsSsm');

  return ssm.getParameters({ WithDecryption: true, Names: paramNames }).promise();
}

// extract the values found in SSM
function extractKeys (keys, ssmParameters) {
  return keys.reduce((obj, key) => {
    try {
      obj[key] = ssmParameters.Parameters.find(p => p.Name.endsWith(key)).Value;
    } catch (e) {
      // handle any missing/undefined parameters without causing a fuss (yet).
      dependencyService('logger').error(e.message);
      obj[key] = null;
    }
    return obj;
  }, {});
}
