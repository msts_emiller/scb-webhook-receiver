'use strict';

const dependencyService = require('./dependency-service');

// Public export symbols
module.exports = {
  handleWebhook,

  // Exported for testing - Not intended to be called directly
  internals: {
    enqueueToTopic,
  }
};

const MESSAGE_TYPE_TOPICS = {
  'account-status': {
    name: 'account-status',
    arnEnvKey: 'accountStatusAdviceSNSTopic',
  },
  'credit-debit-advice': {
    name: 'credit-debit-advice',
    arnEnvKey: 'creditDebitAdviceSNSTopic',
  },
  'payment/transaction-status': {
    name: 'payment-transaction-status',
    arnEnvKey: 'paymentTransactionStatusSNSTopic',
  },
};

function handleWebhook (messageType, body, signature) {
  const { config } = dependencyService('config');
  const EncryptDecrypt = dependencyService('encryptDecrypt');
  const topic = MESSAGE_TYPE_TOPICS[messageType];

  if ( topic === undefined )
  {
    // statusCode = 400;
    return Promise.reject(
      new Error('Unrecognized message type')
    );
  }

  const payload = JSON.parse(body);

  return config(['rsa-private-key'])                                    // Get the PEM encoded private key from SSM
    .then(h => h['rsa-private-key'])                                    // Return just the key
    .then(pem => new EncryptDecrypt(pem))                               // Build the encryption module
    .then(decoder => decoder.decrypt(payload))                          // Decrypt the payload
    .then(message => validateSignature(message, signature))             // TODO: validate signature
    .then(message => enqueueToTopic(topic, message))                    // Publish to SNS
    .then(() => ({ message: 'Successfully received ' + messageType })); // Return a success
}

function validateSignature (message, signature) {
  dependencyService('logger').debug(`message: ${message}`);
  dependencyService('logger').debug(`signature: ${signature}`);
  // TODO actually validate the message
  return message;
}

function enqueueToTopic (topic, message) {
  const sns = dependencyService('awsSns');
  const topicArn = process.env[topic.arnEnvKey];

  let params = {
    TopicArn: topicArn,
    Subject: topic.name, // TODO needed for email testing, should we make it more informative or remove it?
    Message: message,
  };

  return sns.publish(params).promise();
}
