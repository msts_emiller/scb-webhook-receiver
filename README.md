# webhook-receiver

Standard Chartered Bank Integration Webhook Receiver

## Permissions Setup

### If you do not have an AWS account
Open a Cherwell ticket ...

### SSM Key Access
Open a Cherwell ticket requesting the following policies be added to your account:
```
TBD
```

Once this is complete ask a key admin for the project to grant you decryption access via:

IAM > Encryption keys > search for TBD > click on staging > click Add under Key Users

## Installation for developers

After cloning this project, you'll need to do the following additional steps.

##### Install node

Make sure you are running node 8.10. If you're using node version manager this is as simple as:

```bash
nvm install 8.10
```

##### Install aws-cli/aws-mfa

```bash
pip install awscli --upgrade --user
gem install aws-mfa --user-install
```

Add `$HOME/.local/bin` and `$HOME/.gem/ruby/{version}/bin` to your path

```bash
aws configure
# (Go to AWS console to generate items for the prompts)
aws-mfa echo
```

##### Install node modules

From the root of the repository:

```bash
yarn
```

## Developer workflow

TODO

#### How to test

The service code has unit tests written using the `mocha` and `chai` test frameworks.
These can be run using `yarn test` or by directly invoking mocha `mocha`.

#### How to deploy the API with Serverless

Short version: `aws-mfa sls deploy`.

The `--stage <stage_name>` option allows you to deploy to a stage other than staging.

An example of a full deployment to work in progress stage is as follows:

```bash
export OPS_GENIE_ENDPOINT=https://api.opsgenie.com/v1/json/cloudwatch?apiKey=REAL_API_KEY_GOES_HERE
yarn
aws-mfa sls deploy --stage BCA-00000
```

#### How to push the Docker image

If you have made changes to the Dockerfile or if the image is not already in artifactory, you will need to build, tag, and push the docker image.
You must first download docker and have it running on your machine. You can download it here https://www.docker.com/get-started
Once it is running, you must login to Docker

```bash
cd deploy
docker login docker.msts.com
```

After you are logged in, you must build the docker image

```bash
docker build --build-arg AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY --build-arg AWS_DEFAULT_REGION  -t scbi-webhook-receiver .
```

After building, tag the image and push the image. For staging:

```bash
docker tag scbi-webhook-receiver docker.msts.com/scbi-webhook-receiver:staging
docker push docker.msts.com/scbi-webhook-receiver:staging
```

For prod:

```bash
docker tag scbi-webhook-receiver docker-prod.msts.com/scbi-webhook-receiver:prod
docker push docker-prod.msts.com/scbi-webhook-receiver:prod
```

#### How to tear down a test API with Serverless

```bash
aws-mfa sls remove --stage BCA-00000
```

## Required AWS infrastructure

The following things must be in place for all long-lived stages of this
service, but **are NOT created** by Serverless deploys:

- IAM Role: `TBD-<stage>-lambda`
- IAM/KMS Encryption Key: `TBD-<stage>`, granted to the above IAM role
- SSM Parameters:
    - `TBD.<stage>.private-rsa-key` (should be SecureString secured with the above IAM/KMS encryption key)

### Alerts

Alerts to the team are being handled by the serverless-plugin-aws-alerts. This creates an SNS topic (if the environment
variable below exists) and is currently configured to send the alert to an https endpoint. An environment variable needs
to be set and the stage needs to be configured in serverless.yml if you expect to receive alerts from AWS.

```sh
export OPS_GENIE_ENDPOINT=https://api.opsgenie.com/v1/json/cloudwatch?apiKey=REAL_API_KEY_GOES_HERE
```
