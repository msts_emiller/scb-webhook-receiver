'use strict';

const dependencyService = require('./dependency-service');
const crypto = dependencyService('crypto');
const pki = dependencyService('pki');

class EncryptDecrypt {
  constructor (pemPrivateKey, pemPublicKey) {
    if (!pemPrivateKey) {
      throw Error('Missing RSA Private Key');
    }
    this.rsa_private = pemPrivateKey;
    this.rsa_public = pemPublicKey || this.constructor.extractPublicKey(pemPrivateKey);
  }

  static extractPublicKey (pemPrivateKey) {
    let privateKey = pki.privateKeyFromPem(pemPrivateKey);
    let publicKey  = pki.setRsaPublicKey(privateKey.n, privateKey.e);

    if (!publicKey) {
      throw Error('Missing RSA Public Key');
    }

    return pki.publicKeyToPem(publicKey);
  }

  encrypt_aes (unencrypted_payload, aes_key) {
    const IV =  Buffer.alloc(16); // a NULL IV is bad, but SCB has defined this in their code
    let cipher = crypto.createCipheriv('AES-256-CBC', aes_key, IV);
    let crypted = cipher.update(unencrypted_payload, 'utf8', 'base64');
    crypted += cipher.final('base64');
    return crypted;
  }

  decrypt_aes(encrypted_payload) {
    const IV =  Buffer.alloc(16); // a NULL IV is bad, but SCB has defined this in their code
    let cipherName = this.aes_key.length == 16 ? 'AES-128-CBC' : 'AES-256-CBC';
    let decipher = crypto.createDecipheriv(cipherName, this.aes_key, IV);
    let deciphered = decipher.update(encrypted_payload, 'base64');
    deciphered += decipher.final();
    return deciphered;
  }

  encrypt_rsa (unencrypted_payload) {
    // this defaults to RSA_PKCS1_PADDING, but we should be explicit
    return crypto.publicEncrypt(
      { key: this.rsa_public, padding: crypto.constants.RSA_PKCS1_PADDING },
      Buffer.from(unencrypted_payload, "utf8")
    ).toString("base64");
  }

  decrypt_rsa (encrypted_payload) {
    // this defaults to RSA_PKCS1_OAEP_PADDING, which does not match SCB
    return crypto.privateDecrypt(
      { key: this.rsa_private, padding: crypto.constants.RSA_PKCS1_PADDING },
      encrypted_payload
    );
  }

  encrypt (unencrypted_json) {
    let aes_key = crypto.randomBytes(32);
    let encrypted_key, encrypted_value;

    try {
      encrypted_key = this.encrypt_rsa(aes_key);
    } catch (e) {
      dependencyService('logger').error("Unable to encrypt key:", e);
      throw Error("Unable to encrypt key.");
    }

    try {
      encrypted_value = this.encrypt_aes(unencrypted_json, aes_key);
    } catch (e) {
      dependencyService('logger').error("Unable to encrypt value:", e);
      throw Error("Unable to encrypt value.");
    }

    return {
      "key": encrypted_key,
      "value": encrypted_value
    };
  }

  decrypt (encrypted_json) {
    /*
     * encrypted_json = {
     *    key: <RSA-encrypted AES key>,
     *    value: <AES-encrypted string that can be JSON.parsed into a object>
     * }
     */
    let encrypted_AES_decryption_key = Buffer.from(encrypted_json["key"], "base64");
    let encrypted_payload = Buffer.from(encrypted_json["content"] || encrypted_json["value"], "base64");

    try {
      this.aes_key = this.decrypt_rsa(encrypted_AES_decryption_key);
    } catch (e) {
      dependencyService('logger').error("Unable to decipher key:", e);
      throw Error("Malformed payload: unable to decipher key.");
    }

    try {
      return this.decrypt_aes(encrypted_payload);
    } catch (e) {
      dependencyService('logger').error("Unable to decipher value:", e);
      throw Error("Malformed payload: unable to decipher value.");
    }
  }

}

module.exports = EncryptDecrypt;
