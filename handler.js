'use strict';

const dependencyService = require('./dependency-service');
const { handleWebhook } = require('./webhook');

const parseBody = event => event.isBase64Encoded ? Buffer.from(event.body, 'base64') : event.body;

module.exports.hello = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }),
  };

  callback(null, response);
};

module.exports.webhook = (event, context, callback) => {
  if (event.source === 'serverless-plugin-warmup') {
    dependencyService('logger').log('Lambda is warm');
    return callback(null, 'Lambda is warm');
  }

  // dependencyService('logger').info("event:", JSON.stringify(event));

  // Determine suffix being used
  const messageType = event.pathParameters.messageType;
  const body = parseBody(event);
  const signature = event.headers.Signature;

  handleWebhook(messageType, body, signature).then(
    result => {
      callback(null, { statusCode: 200, body: JSON.stringify(result) });
    },
    rejection => {
      callback(null, { statusCode: 400, body: 'Failure processing webhook: ' + rejection.toString() });
    },
  );
};
