'use strict';

const dependencies = {
  awsSns () {
    const aws = require('aws-sdk');
    return new aws.SNS({ apiVersion: '2010-03-31' });
  },
  awsSsm () {
    const aws = require('aws-sdk');
    return new aws.SSM();
  },
  config () {
    return require('./config');
  },
  crypto () {
    return require("crypto");
  },
  pki () {
    return require('node-forge').pki;
  },
  encryptDecrypt () {
    return require('./encrypt_decrypt');
  },
  logger () {
    return console; // TODO some day maybe add some formatting/tagging
  }
};

let loadedDependencies = {};

function dependencyService (name) {
  if (loadedDependencies[name] === undefined) {
    dependencyServiceSetDefault(name);
  }
  return loadedDependencies[name];
}

function dependencyServiceInject (name, dependency) {
  loadedDependencies[name] = dependency;
}

function dependencyServiceSetDefault (name) {
  if (dependencies[name]) {
    dependencyServiceInject(name, dependencies[name]());
  }
}

module.exports        = dependencyService;
module.exports.inject = dependencyServiceInject;
