#!/usr/bin/env node

const fs = require('fs');
const dependencyService = require('../dependency-service');
const EncryptDecrypt = dependencyService('encryptDecrypt');

const args = process.argv.slice(2);

if (args.length < 2) {
  console.log(`USAGE: ./tools/encrypt.js <pemFile> <encrypted_message>`);
  process.exit(1);
}

const [ pemFile, encrypted_message ] = args;

const pemData = fs.readFileSync(pemFile, 'utf8');

const encdec = new EncryptDecrypt(pemData);

let message = encdec.decrypt(JSON.parse(encrypted_message));

console.log(message);
