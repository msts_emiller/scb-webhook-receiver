'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const fs = require('fs');
const { pki } = require('node-forge');
const dependencyService = require('../dependency-service');

chai.use(sinonChai);

const expect = chai.expect;

const EncryptDecrypt = require('../encrypt_decrypt');

const pemPayload = fs.readFileSync('test/test_private.pem', 'utf8');


function generate_encrypted_text(plaintext) {
  let private_key = pki.privateKeyFromPem(pemPayload);
  let public_key = pki.setRsaPublicKey(private_key.n, private_key.e);
  let encoder = new EncryptDecrypt(pemPayload, pki.publicKeyToPem(public_key));
  return encoder.encrypt(plaintext);
}

function decode_encrypted_text(ciphertext) {
  let private_key = pki.privateKeyFromPem(pemPayload);
  let public_key = pki.setRsaPublicKey(private_key.n, private_key.e);
  let decoder = new EncryptDecrypt(pemPayload, pki.publicKeyToPem(public_key));
  return decoder.decrypt(ciphertext);
}

describe('encrypt_decrypt', function () {
  beforeEach(function () {
    const loggerStub = {
      error: sinon.stub(),
    };
    dependencyService.inject('logger', loggerStub);
  });

  afterEach(function () {
    // Clear mocks
    dependencyService.inject('logger');
  });

  it('successfully encodes/decodes a payload', function() {
    let plaintext_data = "abcdefg Happy Birthday!";
    let encrypted = generate_encrypted_text(plaintext_data);

    expect(encrypted["value"]).to.not.equal(plaintext_data);
    expect(encrypted["key"]).to.not.be.null;

    let decrypted = decode_encrypted_text(encrypted);
    expect(decrypted).to.equal(plaintext_data);
  });

  it('gracefully fails if incoming payload was malformed', function() {
    function shuffle(x) {
      //shuffle the encrypted string
      let a = x.split("");
      let n = a.length;
      for(let i = n - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i+1));
        let tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
      }
      return a.join("");
    }

    let plaintext_data = "abcdefg Happy Birthday!";
    let encrypted = generate_encrypted_text(plaintext_data);

    //Muck up value
    encrypted["value"] = shuffle(plaintext_data);
    expect(() => decode_encrypted_text(encrypted)).to.throw(Error, 'Malformed payload: unable to decipher value.');

    //Muck up key
    encrypted = generate_encrypted_text(plaintext_data);
    encrypted["key"] = shuffle(plaintext_data);
    expect(() => decode_encrypted_text(encrypted)).to.throw(Error, 'Malformed payload: unable to decipher key.');
  });
});
