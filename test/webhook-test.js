'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const fs = require('fs');
const dependencyService = require('../dependency-service');

chai.use(chaiAsPromised);
chai.use(sinonChai);

const expect = chai.expect;

const unitToTest = require('../webhook');

const pemFile = fs.readFileSync('test/test_private.pem', 'utf8');
const encrypted_message = (function () {
  const { pki }        = require('node-forge');
  const EncryptDecrypt = require('../encrypt_decrypt');

  let private_key = pki.privateKeyFromPem(pemFile);
  let public_key  = pki.setRsaPublicKey(private_key.n, private_key.e);
  let encoder     = new EncryptDecrypt(pemFile, pki.publicKeyToPem(public_key));

  return encoder.encrypt(JSON.stringify({ secret: 'message' }));
})();

// These are variables set up by AWS when the project is deployed
const overrideEnv = {
  stageName: 'testing',
  accountStatusAdviceSNSTopic: 'arn:aws:sns:us-east-1:434875166128:scbi-webhook-receiver-testing-account-status-advice',
  creditDebitAdviceSNSTopic: 'arn:aws:sns:us-east-1:434875166128:scbi-webhook-receiver-testing-credit-debit-advice',
  testSNSTopic: 'arn:aws:sns:us-east-1:434875166128:scbi-webhook-receiver-testing-test-topic'
};

describe('webhook', function () {
  let restoreEnv;
  let snsMock;

  beforeEach(function () {
    // backup and override Env
    restoreEnv = {};
    Object.keys(overrideEnv).forEach((key) => {
      restoreEnv[key] = process.env[key];
      process.env[key] = overrideEnv[key];
    });

    // Add mocks
    const resultStub = {
      promise: sinon.stub().resolves({})
    };
    snsMock = {
      publish: sinon.stub().returns(resultStub),
    };
    dependencyService.inject('awsSns', snsMock);

    const configStub = {
      config: sinon.stub().resolves({ 'rsa-private-key': pemFile }),
    };
    dependencyService.inject('config', configStub);

    const loggerStub = {
      debug: sinon.stub(),
    };
    dependencyService.inject('logger', loggerStub);
  });

  afterEach(function () {
    // resore Env
    Object.keys(overrideEnv).forEach((key) => {
      process.env[key] = restoreEnv[key];
    });
    restoreEnv = undefined;

    // Clear mocks
    dependencyService.inject('awsSns');
    dependencyService.inject('config');
    dependencyService.inject('logger');
  });

  describe('handleWebhook', function () {
    it('rejects if the message type is not known', function () {
      return expect(unitToTest.handleWebhook('unknown', 'ignore'))
        .to.eventually.be.rejectedWith(Error, 'Unrecognized message type');
    });

    it('enqueues known message types', async () => {
      await unitToTest.handleWebhook('credit-debit-advice', JSON.stringify(encrypted_message));

      expect(snsMock.publish)
        .to.have.been.calledOnceWith({
          TopicArn: 'arn:aws:sns:us-east-1:434875166128:scbi-webhook-receiver-testing-credit-debit-advice',
          Subject: 'credit-debit-advice',
          Message: JSON.stringify({ secret: 'message' }),
        });
    });

    it('resolves to a success message for known message types',  () => {
      return expect(unitToTest.handleWebhook('credit-debit-advice', JSON.stringify(encrypted_message)))
        .to.eventually.become({ message: 'Successfully received credit-debit-advice' });
    });

    it('resolves to a success message for proxy resource message types', () => {
      return expect(unitToTest.handleWebhook('payment/transaction-status', JSON.stringify(encrypted_message)))
        .to.eventually.become({ message: 'Successfully received payment/transaction-status' });
    });
  });

  describe('enqueueToTopic', function () {
    it('sends a message to an SNS topic', async function () {
      const topic = {
        name: 'test-topic',
        arnEnvKey: 'testSNSTopic',
      };
      const message = { 'test': 'message' };

      await unitToTest.internals.enqueueToTopic(topic, message);

      expect(snsMock.publish)
        .to.have.been.calledOnceWith({
          TopicArn: 'arn:aws:sns:us-east-1:434875166128:scbi-webhook-receiver-testing-test-topic',
          Subject: 'test-topic',
          Message: { 'test': 'message' },
        });
    });
  });
});
