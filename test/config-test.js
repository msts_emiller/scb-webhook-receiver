'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');

const unitToTest = require('../config');

describe('config', function () {
  describe('config', function () {
    it('needs tests');
  });

  describe('optional', function () {
    it('needs tests');
  });

  describe('getParams', function () {
    it('needs tests');
  });

  describe('mapKeys', function () {
    it('returns and empty array for an empty array', function () {
      expect(unitToTest.internals.mapKeys([])).to.deep.equal([]);
    });

    it('prefixes keys with the project and staging for non-production stages', function () {
      const inputKeys    = ['foo', 'bar', 'some-complex.key-name'];
      const expectedKeys = [
        'scbi.staging.foo',
        'scbi.staging.bar',
        'scbi.staging.some-complex.key-name',
      ];
      expect(unitToTest.internals.mapKeys(inputKeys)).to.deep.equal(expectedKeys);
    });

    it('prefixes keys with the project and prod for the production stage', function () {
      const inputKeys    = ['foo', 'bar', 'some-complex.key-name'];
      const expectedKeys = [
        'scbi.prod.foo',
        'scbi.prod.bar',
        'scbi.prod.some-complex.key-name',
      ];
      const restoreStage = process.env.stageName;
      process.env.stageName = 'prod';
      expect(unitToTest.internals.mapKeys(inputKeys)).to.deep.equal(expectedKeys);
      process.env.stageName = restoreStage;
    });
  });

  describe('extractKeys', function () {
    it('returns an empty object if no keys are passed', function () {
      const keys = [];
      const ssmParameters = { Parameters: [] };
      const expected = {};
      expect(unitToTest.internals.extractKeys(keys, ssmParameters)).to.deep.equal(expected);
    });

    it('returns an object of all nulls if no params are passed', function () {
      const keys = ['foo', 'bar'];
      const ssmParameters = { Parameters: [] };
      const expected = {
        foo: null,
        bar: null,
      };
      var logStub = sinon.stub(console, "error");
      expect(unitToTest.internals.extractKeys(keys, ssmParameters)).to.deep.equal(expected);
      logStub.restore();
    });

    it('returns an object with the original keys instead of full SSM names', function () {
      const keys = ['foo', 'bar'];
      const ssmParameters = {
        Parameters: [
          { Name: 'scbi.staging.foo', Value: '1234', },
          { Name: 'scbi.staging.bar', Value: '8765', },
        ]
      };
      const expected = {
        foo: '1234',
        bar: '8765',
      };
      expect(unitToTest.internals.extractKeys(keys, ssmParameters)).to.deep.equal(expected);
    });
  });
});
