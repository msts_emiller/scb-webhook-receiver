'use strict';

const expect = require('chai').expect;

const unitToTest = require('../dependency-service');

describe('When using the dependency service', function () {
  it('returns a default service when none is injected', function () {
    const realConfig = require('../config');
    const depsConfig = unitToTest('config');
    expect(depsConfig).to.equal(realConfig);
  });

  it('returns the injected service', function () {
    const fakeConfig = function () {};

    unitToTest.inject('config', fakeConfig);

    const depsRequest = unitToTest('config');
    const realConfig = require('../config');
    
    expect(depsRequest).to.equal(fakeConfig)
      .and.not.equal(realConfig);
  });


  it('returns a default service when an injected service is cleared', function () {
    const fakeConfig = function () {};

    unitToTest.inject('config', fakeConfig);
    unitToTest.inject('config');

    const depsRequest = unitToTest('config');
    const realConfig = require('../config');
    
    expect(depsRequest).to.equal(realConfig)
      .and.not.equal(fakeConfig);
  });
});
